## Java Programming Challenge

*Note: This task should take no longer than 3-4 hours at the most.*
*Note: You will have 4 days from when this link was sent to complete the challenge*

## Task

1. Fork this repository
2. Create a *source* folder to contain your code. 
3. In the *source* directory, please create a Java 1.8 web application using a framework (Spark, Spring, Play, etc.)
4. Your application should accomplish the following:  
  - Connect to the [Github API](http://developer.github.com/)  
  - Find the [google/gson](https://github.com/google/gson) repository  
  - Find the 50 most recent commits  
  - Create a model and store the commits in a relational database.  
  - Create a basic template and utilize a CSS framework (Bootstrap, Foundation, etc.)  
  - Create a route and view which displays the commit hash, comment, author, and date  
  - If the commit hash ends in an even number, color that row "ASU Maroon", otherwise use "ASU Gold"  
  
### Once Complete
1. Create a SETUP.md in the root directory with instructions on how to set up the application
2. Create a pull request. We'll review soon after. 

## What We're Looking For
  - Demonstration of MVC patterns
  - Database design 
  - Ability to create basic model and retrieve information from the database
  - Modern frontend practices
  
## Bonus Points if you use:
- Gradle/Maven
- 3rd party libraries
- Vue.js, React
- NPM/Yarn, Webpack, SCSS